#!/usr/bin/env bash

set -e


echo -e "\n Building docker images"
docker/build.sh

echo -e "\n Building cloud services"
find ./cloud -type f -mindepth 2 -maxdepth 2 -name 'pom.xml' | rev | cut -d '/' -f 2 | rev | while read SERVICE_NAME;
do
    mvn --file cloud/pom.xml clean package docker:build -pl $SERVICE_NAME
done
